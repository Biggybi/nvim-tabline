local utils = require("tabline.utils.utils")
local config = require("tabline.config").options

local M = {}
-- 󱓺 󱨈  󰤌
local icons = {
  leader = "",
  ctrl_leader = "",
  normal = "",
  global = "",
  pending = "", -- 
  visual = "",
  visual_block = "",
  visual_line = "",
  insert = "", --   
  select = "",
  replace = "",
  command = "",
  terminal = "",
  shell = "",
  confirm = "",
  more = "",
  fold = "",
  indent_add = "",
  indent_remove = "",
  indent_auto = "", --  
  wincmd = "", --      
  mark = "",
  search = "",
  search_until_forward = "",
  search_until_backward = "",
  search_exact_forward = "",
  search_exact_backward = "",
  macro = "",
  copy = "",
  copy_comment = "",
  comment = "",
  delete = "",
  change = "",
  toggle = "",
  git = "",
  find = "",
  debug = "",
  file = "",
  various = "",
  trouble = "",
  todo = "",
  make = "",
  message = "",
  path = "",
  count = "",
  surround = "",
  sleep = "",
  trim = "",
}

---@class Mode
---@field name string
---@field group string
---@field icon? string
---@field color string

---@type table<string, Mode>
M.modes = {
  ["n"] = {
    name = "NORMAL",
    group = "n",
    icon = icons["normal"],
    color = "BesLineModeNormal",
  },
  ["niI"] = {
    name = "NORMAL",
    group = "n",
    icon = icons["normal"],
    color = "BesLineModeNormal",
  },
  ["niR"] = {
    name = "NORMAL",
    group = "n",
    icon = icons["normal"],
    color = "BesLineModeNormal",
  },
  ["niV"] = {
    name = "NORMAL",
    group = "n",
    icon = icons["normal"],
    color = "BesLineModeNormal",
  },
  ["nt"] = {
    name = "NORMAL",
    group = "n",
    icon = icons["normal"],
    color = "BesLineModeNormal",
  },
  ["ntT"] = {
    name = "NORMAL",
    group = "n",
    icon = icons["normal"],
    color = "BesLineModeNormal",
  },
  ["no"] = {
    name = "O-PENDING",
    group = "o",
    icon = icons["pending"],
    color = "BesLineModePending",
  },
  ["nov"] = {
    name = "O-PENDING",
    group = "o",
    icon = icons["pending"],
    color = "BesLineModePending",
  },
  ["noV"] = {
    name = "O-PENDING",
    group = "o",
    icon = icons["pending"],
    color = "BesLineModePending",
  },
  ["no\22"] = {
    name = "O-PENDING",
    group = "o",
    icon = icons["pending"],
    color = "BesLineModePending",
  },
  ["nft"] = {
    name = "O-SEARCH",
    group = "o",
    icon = icons["terminal"],
    color = "BesLineModePendingSearch",
  },
  ["v"] = {
    name = "VISUAL",
    group = "v",
    icon = icons["visual"],
    color = "BesLineModeVisual",
  },
  ["vs"] = {
    name = "VISUAL",
    group = "v",
    icon = icons["visual"],
    color = "BesLineModeVisual",
  },
  ["V"] = {
    name = "V-LINE",
    group = "v",
    icon = icons["visual_line"],
    color = "BesLineModeVisualLine",
  },
  ["Vs"] = {
    name = "V-LINE",
    group = "v",
    icon = icons["visual_line"],
    color = "BesLineModeVisualLine",
  },
  ["\22"] = {
    name = "V-BLOCK",
    group = "v",
    icon = icons["visual_block"],
    color = "BesLineModeVisualBlock",
  },
  ["\22s"] = {
    name = "V-BLOCK",
    group = "v",
    icon = icons["visual_block"],
    color = "BesLineModeVisualBlock",
  },
  ["s"] = {
    name = "SELECT",
    group = "s",
    icon = icons["select"],
    color = "BesLineModeSelect",
  },
  ["S"] = {
    name = "S-LINE",
    group = "s",
    icon = icons["select"],
    color = "BesLineModeSelectLine",
  },
  ["\19"] = {
    name = "S-BLOCK",
    group = "s",
    icon = icons["select"],
    color = "BesLineModeSelectBlock",
  },
  ["i"] = {
    name = "INSERT",
    group = "i",
    icon = icons["insert"],
    color = "BesLineModeInsert",
  },
  ["ic"] = {
    name = "INSERT",
    group = "i",
    icon = icons["insert"],
    color = "BesLineModeInsert",
  },
  ["ix"] = {
    name = "INSERT",
    group = "i",
    icon = icons["insert"],
    color = "BesLineModeInsert",
  },
  ["R"] = {
    name = "REPLACE",
    group = "r",
    icon = icons["replace"],
    color = "BesLineModeReplace",
  },
  ["Rc"] = {
    name = "REPLACE",
    group = "r",
    icon = icons["replace"],
    color = "BesLineModeReplace",
  },
  ["Rx"] = {
    name = "REPLACE",
    group = "r",
    icon = icons["replace"],
    color = "BesLineModeReplace",
  },
  ["r"] = {
    name = "REPLACE",
    group = "r",
    icon = icons["replace"],
    color = "BesLineModeReplace",
  },
  ["Rv"] = {
    name = "V-REPLACE",
    group = "v",
    icon = icons["replace"],
    color = "BesLineModeVisualReplace",
  },
  ["Rvc"] = {
    name = "V-REPLACE",
    group = "v",
    icon = icons["replace"],
    color = "BesLineModeVisualReplace",
  },
  ["Rvx"] = {
    name = "V-REPLACE",
    group = "v",
    icon = icons["replace"],
    color = "BesLineModeVisualReplace",
  },
  ["c"] = {
    name = "COMMAND",
    group = "c",
    icon = icons["command"],
    color = "BesLineModeCommand",
  },
  ["cv"] = {
    name = "EX",
    group = "c",
    icon = icons["command"],
    color = "BesLineModeCommand",
  },
  ["ce"] = {
    name = "EX",
    group = "c",
    icon = icons["command"],
    color = "BesLineModeCommand",
  },
  ["rm"] = {
    name = "MORE",
    group = "u",
    icon = icons["more"],
    color = "BesLineModeMore",
  },
  ["r?"] = {
    name = "CONFIRM",
    group = "u",
    icon = icons["confirm"],
    color = "BesLineModeConfirm",
  },
  ["!"] = {
    name = "SHELL",
    group = "t",
    icon = icons["shell"],
    color = "BesLineModeShell",
  },
  ["t"] = {
    name = "TERMINAL",
    group = "t",
    icon = icons["terminal"],
    color = "BesLineModeTerminal",
  },
}

---@type table<string, Mode>
M.extra_mode = {
  [":"] = {
    name = "CMDLINE-CMD",
    group = "c",
    icon = ":",
    color = "BesLineModeCommand",
  },
  ["/"] = {
    name = "CMDLINE-SEARCH",
    group = "c",
    icon = "/",
    color = "BesLineModeSearch",
  },
  -- space
  ["<20>"] = {
    name = "_-PENDING",
    group = "o",
    icon = icons["leader"],
    color = "BesLineModeLeader",
  },
  -- ctrl-space
  ["<80>ü^D<20>"] = {
    name = "*-PENDING",
    group = "o",
    icon = icons["ctrl_leader"],
    color = "BesLineModeCtrlLeader",
  },
  ["<20>f"] = {
    name = "F-SEARCH",
    group = "o",
    icon = icons["find"],
    color = "BesLineModePendingSearch",
  },
  ["f"] = {
    name = "F-SEARCH",
    group = "o",
    icon = icons["search_exact_forward"],
    color = "BesLineModePendingSearch",
  },
  ["t"] = {
    name = "T-SEARCH",
    group = "o",
    icon = icons["search_until_forward"],
    color = "BesLineModePendingSearch",
  },
  ["F"] = {
    name = "F-SEARCH",
    group = "o",
    icon = icons["search_exact_backward"],
    color = "BesLineModePendingSearch",
  },
  ["T"] = {
    name = "T-SEARCH",
    group = "o",
    icon = icons["search_until_backward"],
    color = "BesLineModePendingSearch",
  },
  ["["] = {
    name = "[-PENDING",
    group = "o",
    icon = "[",
    color = "BesLineModePendingSearch",
  },
  ["]"] = {
    name = "]-PENDING",
    group = "o",
    icon = "]",
    color = "BesLineModePendingSearch",
  },
  ["d"] = {
    name = "D-PENDING",
    group = "o",
    icon = icons["delete"],
    color = "BesLineModePending",
  },
  ['"_d'] = {
    name = "D-PENDING",
    group = "o",
    icon = icons["delete"],
    color = "BesLineModePending",
  },
  ["c"] = {
    name = "C-PENDING",
    group = "o",
    icon = icons["change"],
    color = "BesLineModePending",
  },
  ["y"] = {
    name = "Y-PENDING",
    group = "o",
    icon = icons["copy"],
    color = "BesLineModePending",
  },
  ['"+y'] = {
    name = "Y-PENDING",
    group = "o",
    icon = icons["copy"],
    color = "BesLineModePending",
  },
  ["gc"] = {
    name = "YC-PENDING",
    group = "o",
    icon = icons["comment"],
    color = "BesLineModePending",
  },
  ["yc"] = {
    name = "YC-PENDING",
    group = "o",
    icon = icons["copy_comment"],
    color = "BesLineModePending",
  },
  ["="] = {
    name = "=-PENDING",
    group = "o",
    icon = icons["indent_auto"],
    color = "BesLineModePending",
  },
  ["<"] = {
    name = "<-PENDING",
    group = "o",
    icon = icons["indent_remove"],
    color = "BesLineModePending",
  },
  [">"] = {
    name = ">-PENDING",
    group = "o",
    icon = icons["indent_add"],
    color = "BesLineModePending",
  },
  ["g"] = {
    name = "G-PENDING",
    group = "o",
    icon = icons["global"],
    color = "BesLineModePendingSearch",
  },
  ["gs"] = {
    name = "GS-PENDING",
    group = "o",
    icon = icons["sleep"],
    color = "BesLineModePendingSearch",
  },
  ["<20>g"] = {
    name = "GIT-PENDING",
    group = "o",
    icon = icons["various"],
    color = "BesLineModeGit",
  },
  ["'"] = {
    name = "'-PENDING",
    group = "o",
    icon = icons["mark"],
    color = "BesLineModePendingSearch",
  },
  ["q"] = {
    name = "Q-PENDING",
    group = "o",
    icon = icons["macro"],
    color = "BesLineModePending",
  },
  ["z"] = {
    name = "Z-PENDING",
    group = "o",
    icon = icons["fold"],
    color = "BesLineModePending",
  },
  ["m"] = {
    name = "M-PENDING",
    group = "o",
    icon = icons["mark"],
    color = "BesLineModePending",
  },
  ["gh"] = {
    name = "GIT-PENDING",
    group = "o",
    icon = icons["git"],
    color = "BesLineModeGit",
  },
  -- ctrl-w
  ["^W"] = {
    name = "W-PENDING",
    group = "o",
    icon = icons["wincmd"],
    color = "BesLineModePending",
  },
  ["^N"] = {
    name = "N-PENDING",
    group = "o",
    icon = icons["path"],
    color = "BesLineModePending",
  },
  ["yo"] = {
    name = "YO-PENDING",
    group = "o",
    icon = icons["toggle"],
    color = "BesLineModePending",
  },
  ["<20>d"] = {
    name = "D-PENDING",
    group = "o",
    icon = icons["debug"],
    color = "BesLineModePending",
  },
  ["<20>e"] = {
    name = "D-PENDING",
    group = "o",
    icon = icons["file"],
    color = "BesLineModePending",
  },
  ["<20>s"] = {
    name = "D-PENDING",
    group = "o",
    icon = icons["various"],
    color = "BesLineModePending",
  },
  ["<20>q"] = {
    name = "D-PENDING",
    group = "o",
    icon = icons["various"],
    color = "BesLineModePending",
  },
  ["<20>t"] = {
    name = "D-PENDING",
    group = "o",
    icon = icons["todo"],
    color = "BesLineModePending",
  },
  ["<20>c"] = {
    name = "D-PENDING",
    group = "o",
    icon = icons["make"],
    color = "BesLineModePending",
  },
  ["<20>m"] = {
    name = "D-PENDING",
    group = "o",
    icon = icons["message"],
    color = "BesLineModePending",
  },
  ["g@l"] = {
    name = "SURROUND-PENDING",
    group = "o",
    icon = icons["surround"],
    color = "BesLineModePending",
  },
  -- ["g@i"] = {
  --   name = "SURROUND-PENDING",
  --   group = "o",
  --   icon = icons["surround"],
  --   color = "BesLineModePending",
  -- },
  ["ys"] = {
    name = "SURROUND-PENDING",
    group = "o",
    icon = icons["surround"],
    color = "BesLineModePending",
  },
  ["dx"] = {
    name = "TRIM-PENDING",
    group = "o",
    icon = icons["trim"],
    color = "BesLineModePending",
  },
}

M.mode_count = {
  name = "COUNT",
  group = "o",
  icon = icons["count"],
  color = "BesLineModePending",
}

---@return Mode | nil
function M.get_mode()
  local mode_code = vim.api.nvim_get_mode().mode
  local mode = M.modes[mode_code]
  if not mode then return end
  if mode.group == "n" or mode.group == "c" then
    local input = vim.api.nvim_eval_statusline("%S", {}).str
    if utils.is_number_string(input) then return M.mode_count end
    local input_cmd = utils.strip_leading_number(input)
    local extra_mode = M.extra_mode[input_cmd]
    if extra_mode then return extra_mode end
    if
      #input_cmd == 1
      and not vim.tbl_contains(config.statusline.showcmd_ignore, input_cmd)
    then
      return M.modes["no"]
    end
  end
  return mode
end

return M
