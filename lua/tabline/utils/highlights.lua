M = {}

---@class HighlightOptions
---@field sep boolean
---@field modified boolean
---@field active boolean

---@class GetHlName
---@param name string
---@param opts HighlightOptions
function M.get_hl_name(name, opts)
  local sep = opts.sep and "Sep" or ""
  local modified = opts.modified and "Mod" or ""
  local active = opts.active and "Sel" or ""
  return name .. sep .. modified .. active
end

return M
