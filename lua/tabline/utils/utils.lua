local M = {}

---@param s string
function M.is_number_string(s) return s:match("^%d+$") ~= nil end

---@param s string
function M.string_leading_number(s) return s:match("^%d+") end

---@param s string
function M.strip_leading_number(s) return s:match("[^%d].*") or "" end

return M
