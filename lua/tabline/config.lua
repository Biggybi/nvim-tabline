local M = {}
---@class diagnostic_icons
---@field error string
---@field warn string
---@field info string
---@field hint string

---@class session_icon
---@field active string
---@field inactive string

---@class directory_icon
---@field git string
---@field default string

---@class TablineOptions
---@field reload_on_focus boolean
---@field no_name string
---@field left_separator string
---@field right_separator string
---@field tab_margin number
---@field color_all_icons boolean
---@field show_icon boolean
---@field tab_size number
---@field dir_section_size number
---@field hl_auto_reload boolean
---@field directory_icon directory_icon
---@field session_icon session_icon

---@class StatuslineOptions
---@field reload_events_active string[]
---@field reload_events_inactive string[]
---@field hl_auto_reload boolean
---@field left_separator string
---@field right_separator string
---@field mode_icon boolean
---@field mode_style 'short' | 'long' | 'icon'
---@field branch_icon string
---@field progress_icon string
---@field git_diff_number boolean
---@field center_path boolean
---@field diagnostic_icons diagnostic_icons
---@field diagnostic_min_severity string
---@field lsp_icon string
---@field copilot_icon string
---@field search_count_icon string
---@field macro_icon string
---@field showcmd_icon string
---@field showcmd_icon_visual string
---@field showcmd_ignore string[]
---@field showcmd_ignore_leader string[]
---@field showcmd_min_padding number
---@field showcmd_cmd_autoclear boolean
---@field showcmd_cmd_clear_escape boolean
---@field showcmd_cmd_clear_ignore boolean

---@class StatuscolumnOptions
---@field left_separator string
---@field right_separator string
---@field right_padding number
---@field min_number_width number
---@field wrap_symbol string
---@field wrap_symbol_last string
---@field ft_blacklist string[]

---@class ExtrasOptions
---@field mode_sync_hl string[]

---@class BeLineOptions
---@field tabline TablineOptions
---@field statusline StatuslineOptions
---@field statuscolmn StatuscolumnOptions
---@field extras ExtrasOptions

---@type BeLineOptions
local defaults = {
  tabline = {
    reload_on_focus = false,
    no_name = "[No Name]",
    left_separator = "",
    right_separator = "",
    tab_margin = 1,
    color_all_icons = true,
    show_icon = true,
    tab_size = 30,
    dir_section_size = 15,
    hl_auto_reload = true, -- Reload when colorscheme changes
    directory_icon = {
      git = "",
      default = "",
    },
    session_icon = {
      --        
      active = "",
      inactive = "",
    },
  },
  statusline = {
    reload_events_active = { "WinEnter", "BufWinEnter", "DiagnosticChanged" }, -- FocusGained
    reload_events_inactive = { "WinLeave" }, -- FocusLost
    hl_auto_reload = true,
    left_separator = "",
    right_separator = "",
    mode_icon = true,
    mode_style = "icon",
    branch_icon = " ", -- 
    progress_icon = "", --      
    git_diff_number = false,
    center_path = true,
    diagnostic_icons = {
      error = " ",
      warn = " ",
      hint = " ",
      info = " ",
    },
    lsp_icon = "", --  󰴽     󱞀 󰓆 󱍨 
    copilot_icon = " ",
    diagnostic_min_severity = "info",
    search_count_icon = "",
    macro_icon = "",
    showcmd_icon = " ", --       
    showcmd_icon_visual = " ", --     
    showcmd_ignore = {
      "n",
      "N",
      ":",
      "/",
      "i",
      "v",
      "j",
      "k",
      "h",
      "l",
      "gj",
      "gk",
      "^D",
      "^U",
      "%",
    },
    showcmd_ignore_leader = { "^W" },
    showcmd_min_padding = 5,
    showcmd_cmd_autoclear = true, -- clear showcmd when cmd is executed
    showcmd_cmd_clear_escape = true, -- clear showcmd on escape
    showcmd_cmd_clear_ignore = true, -- clear showcmd on ignored cmds
  },
  statuscolmn = {
    left_separator = "",
    right_separator = "",
    min_number_width = 3,
    right_padding = 1,
    wrap_symbol = "├",
    wrap_symbol_last = "└",
    ft_blacklist = { "snacks_picker_input", "TelescopePrompt" },
  },
  extras = {
    mode_sync_hl = { "BeNLineNr" },
  },
}

function M.setup(user_options)
  ---@type BeLineOptions
  M.options = vim.tbl_deep_extend("force", {}, defaults, user_options)
end

return M
