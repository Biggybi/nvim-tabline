local M = {}
local u_mode = require("tabline.utils.mode")

local utils = require("tabline.utils")
local icons = require("tabline.icons")
local sl_options = require("tabline.config").options.statusline

local function git_dir()
  local dot_git_path = vim.fn.finddir(".git", ".;")
  return dot_git_path
end

local function git_or_parent_dir()
  local dot_git_path = git_dir()
  if dot_git_path ~= "" then return vim.fn.fnamemodify(dot_git_path, ":p:h:h") end

  local parent_path = vim.fn.fnamemodify(vim.fn.expand("%"), ":p:h")
  return parent_path
end

local function is_file_modified() return vim.bo.modified end

local function get_file_path()
  local filepath = vim.fn.expand("%:p")
  if vim.bo.filetype == "fugitive" then return filepath:gsub("^fugitive://", "") end
  return filepath
end

local function get_file_relative_path()
  local filepath = get_file_path()
  local parent = git_or_parent_dir()
  local relative_path = filepath:sub(#parent + 2)
  if relative_path == "" or relative_path == nil then return filepath end
  return relative_path
end

local function parent_name()
  local parent_path = git_or_parent_dir()
  return vim.fn.fnamemodify(parent_path, ":t")
end

local function statusline_mode()
  local mode = u_mode.get_mode() or u_mode.modes["n"]
  local s = ""
  local s_nc_len = 2

  local color =
    vim.api.nvim_get_hl(0, { id = vim.api.nvim_get_hl_id_by_name(mode.color) })
  vim.api.nvim_set_hl(0, "BeSLineEdge", { fg = color.bg })

  s = s .. utils.get_statuslineitem("BeSLineEdge", sl_options.left_separator, false)
  s = s .. utils.get_statuslineitem(mode.color, " ", false)
  if sl_options.mode_icon then
    s = s .. utils.get_statuslineitem(mode.color, mode.icon .. " ")
    s_nc_len = s_nc_len + 3
  end
  if sl_options.mode_style ~= "icon" then
    if sl_options.mode_style == "short" then mode.name = mode.name:sub(1, 1) end
    if mode.name ~= nil and mode.name ~= "" then
      s = s .. utils.get_statuslineitem(mode.color, mode.name, false)
      s_nc_len = s_nc_len + #mode.name
    end
  end
  s = s .. utils.get_statuslineitem(mode.color, " ", false)
  s = s .. utils.get_statuslineitem(mode.color, sl_options.right_separator .. " ", true)
  if vim.b.besl_active == false then
    local sep = utils.get_statuslineitem("BeSLineEdge", sl_options.left_separator, false)
    local pad_right = utils.get_statuslineitem("BeSLine", string.rep(" ", s_nc_len + 1))
    return sep .. pad_right
  end
  -- vim.cmd([[doautocmd User BesLineUser]])
  return s
end

local function parent_dir()
  local dir = parent_name()
  local group = "BeSLineFolder"
  if vim.bo.buftype == "terminal" then
    group = "BeSLineFolderTerminal"
    dir = "term"
  elseif vim.bo.buftype == "help" then
    group = "BeSLineFolderHelp"
    dir = "doc"
  elseif vim.bo.buftype == "quickfix" then
    return ""
  elseif git_dir() ~= "" then
    group = "BeSLineFolderGit"
  end
  return utils.get_statuslineitem(group, dir .. " ", false)
end

local function special_buffer_path()
  local center = sl_options.center_path == true and "%=" or ""
  if utils.is_special_buffer() == false then return end
  local group = "BeSLineFolder"
  local filename = vim.fn.expand("%:t:r")
  if vim.bo.buftype == "terminal" then
    group = "BeSLineFolderTerminal"
    filename = vim.fn.expand("%:f")
  elseif vim.bo.buftype == "help" then
    group = "BeSLineFolderHelp"
    filename = vim.fn.expand("%:t:r")
  elseif vim.bo.buftype == "quickfix" then
    filename = "quickfix"
    group = "BeSLineFolderQuickfix"
    filename = vim.w.quickfix_title or ""
  elseif vim.bo.filetype == "fugitive" then
    filename = get_file_path()
  end
  return utils.get_statuslineitem(group, center .. filename .. " ", false, false)
end

local function path()
  if utils.is_special_buffer() == true then return special_buffer_path() end
  local center = sl_options.center_path == true and "%=" or ""
  local mod = is_file_modified()
  local filepath = center .. get_file_relative_path() .. " "
  return utils.get_statuslineitem("BeSLineFile", filepath, false, mod)
end

local function gitsitns_git_branch()
  if package.loaded["gitsigns"] == nil then return "" end
  if git_dir() == "" or utils.is_special_buffer() == true then return "" end
  if vim.b.gitsigns_status_dict == nil then return "" end
  local branch = vim.b.gitsigns_status_dict.head
  if branch == nil then return "" end
  return branch
end

---- get git branch or head commit hash
--local function system_git_branch()
--	local head = vim.fn.systemlist("git rev-parse --abbrev-ref HEAD")[1]
--	if head == "HEAD" then
--		return nil
--	end
--	return head
--end

--local function system_git_head_commit_hash()
--	return vim.fn.systemlist("git rev-parse --short HEAD")[1]
--end

-----@return string
--local function system_git_head()
--	if git_dir() == "" or utils.is_special_buffer() == true then
--		return ""
--	end
--	local branch = system_git_branch()
--	if branch ~= nil then
--		return branch
--	end
--	local head = system_git_head_commit_hash()
--	if head ~= nil then
--		return head
--	end
--	return ""
--end

local function git_diff_extract_symbols(s)
  if package.loaded["gitsigns"] == nil then return "" end
  if s == nil then return "" end
  local symbols = ""
  for symbol in s:gmatch("[+%-%~]+") do
    symbols = symbols .. symbol
  end
  return symbols
end

local function git_diff()
  local gs_dict = vim.b.gitsigns_status
  if gs_dict == nil then return "" end
  if sl_options.git_diff_number == true then return gs_dict end
  return git_diff_extract_symbols(gs_dict)
end

local function get_connected_clients()
  local actual_curbuf = vim.fn.bufnr(vim.api.nvim_eval_statusline("%f", {}).str)
  local connected_clients_names = vim.tbl_map(
    function(client) return client.name end,
    vim.lsp.get_clients({ bufnr = actual_curbuf })
  )
  return connected_clients_names
end

local function copilot_status()
  if package.loaded["copilot"] == nil then return "" end
  local copilot_connected = require("copilot.client").buf_is_attached()
  if copilot_connected == false then return "" end
  return utils.get_statuslineitem("BeSLineCopilotIcon", sl_options.copilot_icon, false)
end

local function lsp_status()
  local connected_clients = get_connected_clients()
  if #connected_clients == 0 then return "" end
  local s = copilot_status()
  if #connected_clients == 1 then
    return s .. utils.get_statuslineitem("BeSLine", " ", false)
  end
  return s
    .. utils.get_statuslineitem("BeSLineLspIcon", sl_options.lsp_icon .. "  ", false)
end

local function get_lsp_diagnostic_item_count(type)
  local curr_mode = vim.api.nvim_get_mode().mode
  if curr_mode == "i" or curr_mode == "R" or curr_mode == "s" or curr_mode == "S" then
    return 0
  end
  if not vim.diagnostic then return 0 end
  return #vim.diagnostic.get(0, { severity = vim.diagnostic.severity[type] })
end

---@return table
local function get_severity_items()
  local min_severity = sl_options.diagnostic_min_severity
  local base_severity = { error = 4, warn = 3, hint = 2, info = 1 }
  local items = {}
  for severity, index in pairs(base_severity) do
    get_lsp_diagnostic_item_count(string.upper(severity))
    if index >= tonumber(base_severity[min_severity]) then
      items[severity] = get_lsp_diagnostic_item_count(string.upper(severity))
    end
  end
  return items
end

local function lsp_diagnostics_item()
  local items = get_severity_items()
  if items == nil or items == {} then return "" end
  local s = ""
  for _, severity in ipairs({ "error", "warn", "hint", "info" }) do
    local count = items[severity]
    if count > 0 then
      s = s
        .. utils.get_statuslineitem(
          "BeSLineDiagnostic" .. severity,
          sl_options.diagnostic_icons[severity] .. tostring(count) .. " ",
          false
        )
    end
  end
  return s
end

local function qf_index()
  if vim.bo.buftype ~= "quickfix" then return "" end
  local qf_title = vim.api.nvim_eval_statusline("%q", {}).str
  if qf_title and qf_title ~= "" then
    local qf_type = qf_title:lower():match("location") and "ll" or "qf"
    if qf_type == "ll" then return "" end
    local current_qf_index = vim.fn.getqflist({ nr = 0 })
    local last_qf_index = vim.fn.getqflist({ nr = "$" })
    local qf_index_item = string.format("%d/%d", current_qf_index.nr, last_qf_index.nr)
    return utils.get_statuslineitem("BeSLineFolderQuickfix", qf_index_item .. " ", false)
  end
end

local function qf_infos()
  if vim.bo.buftype ~= "quickfix" then return "" end
  local group = "BeSLineQuickfix"
  local qf_title = vim.api.nvim_eval_statusline("%q", {}).str
  local dir = qf_title:lower():match("location") and "loclist" or "quickfix"
  local s = utils.get_statuslineitem(group, sl_options.left_separator, true)
  s = s .. utils.get_statuslineitem(group, dir, false)
  s = s .. utils.get_statuslineitem(group, sl_options.right_separator, true)
  return s .. utils.get_statuslineitem("BeSLine", " ", false)
end

local function is_file_tracked(filepath)
  local result = vim.system({ "git", "ls-files", "--error-unmatch", filepath }):wait()
  return result.code == 0
end

local function git_infos()
  if package.loaded["gitsigns"] == nil then return "" end
  if utils.is_special_buffer() == true or utils.get_gitdir() == nil then return "" end
  local branch = gitsitns_git_branch()
  local diff = git_diff()
  local s = ""
  local hlgroup = "BeSLineGitBranch"
  if not is_file_tracked(get_file_path()) then hlgroup = "BeSLineGitBranchUntracked" end
  s = s .. utils.get_statuslineitem(hlgroup, sl_options.left_separator, true)
  s = s .. utils.get_statuslineitem(hlgroup, sl_options.branch_icon .. branch, false)
  if diff ~= "" then s = s .. utils.get_statuslineitem(hlgroup, " " .. diff, false) end
  s = s .. utils.get_statuslineitem(hlgroup, " ", false)
  s = s .. utils.get_statuslineitem(hlgroup, sl_options.right_separator, true)
  return s .. utils.get_statuslineitem("BeSLine", " ", false)
end

local function progress_percent()
  local curln = tonumber(vim.api.nvim_eval_statusline("%l", {}).str) or 0
  local totln = tonumber(vim.api.nvim_eval_statusline("%L", {}).str) or 0
  if curln <= 1 then return "Top" end
  if curln == totln then return "Bot" end
  local percentage = vim.api.nvim_eval_statusline("%p", {}).str
  return ("%2d%s"):format(percentage, "%%")
end

local function progress_line()
  local l = vim.api.nvim_eval_statusline("%l", {}).str
  local c = vim.api.nvim_eval_statusline("%c", {}).str
  return ("%4s:%-3s "):format(l, c)
end

local function progress()
  local mode = u_mode.get_mode() or u_mode.modes["n"]
  local color = vim.b.besl_active == true and mode.color or "BeSLine"
  local modecolor =
    vim.api.nvim_get_hl(0, { id = vim.api.nvim_get_hl_id_by_name(mode.color) })
  local modecolorNC =
    vim.api.nvim_get_hl(0, { id = vim.api.nvim_get_hl_id_by_name("BeslineModeNormalNC") })
  vim.api.nvim_set_hl(0, "BeSLineEdge", { fg = modecolor.bg })
  vim.api.nvim_set_hl(0, "BeSLineEdgeNC", { fg = modecolorNC.bg })

  local s = ""
  if vim.b.besl_active == false then
    s = s .. utils.get_statuslineitem("BeSLine", "   ", false)
  else
    s = s .. utils.get_statuslineitem(color, sl_options.left_separator, true)
    s = s .. utils.get_statuslineitem(color, sl_options.progress_icon .. " ", false)
  end
  s = s .. utils.get_statuslineitem(color, progress_line() .. progress_percent(), false)
  if vim.b.besl_active == false then
    s = s .. utils.get_statuslineitem("BeSLineEdge", sl_options.right_separator, false)
  else
    s = s .. utils.get_statuslineitem("BeSLineEdge", sl_options.right_separator, false)
  end
  return s
end

local function get_extension()
  return vim.fn.fnamemodify(vim.api.nvim_eval_statusline("%f", {}).str, ":e")
end

local function filetype()
  local color = "BeSLine"
  local extension = get_extension()
  local icon =
    icons.get_devicon(false, vim.api.nvim_eval_statusline("%f", {}).str, extension)
  local ft = vim.bo.filetype
  if ft == "" then return "" end
  local icon_item = utils.get_statuslineitem(color, icon, false)
  local ft_item = utils.get_statuslineitem(color, ft, false)
  return icon_item .. ft_item .. utils.get_statuslineitem("BeSLine", " ", false)
end

local function search_count()
  if vim.o.hlsearch == false or vim.v.hlsearch == 0 then return "" end
  local ok, count = pcall(vim.fn.searchcount, { maxcount = 0 })
  if not ok then return "" end
  if count.incomplete ~= 0 then -- error
    return ""
  end
  if not count or count == "" or not count.total or count.total == 0 then return "" end
  local left_padding = (" "):rep(
    1 + tostring(count.total):len() - tostring(count.current):len()
  )
  local s = utils.get_statuslineitem(
    "BeSLineSearch",
    sl_options.search_count_icon
      .. left_padding
      .. count.current
      .. "/"
      .. count.total
      .. " ",
    false
  )
  return s
end

local function macro_recording()
  local macro = vim.fn.reg_recording()
  if macro == "" then return "" end
  local s = utils.get_statuslineitem(
    "BeSLineMacro",
    sl_options.macro_icon .. " " .. macro .. " ",
    false
  )
  return s
end

local cmd_bak = ""

local function get_showcmd_padding()
  local padding = sl_options.showcmd_min_padding - vim.api.nvim_strwidth(cmd_bak)
  if padding <= 0 then return "" end
  return (" "):rep(padding)
end

local function showcmd()
  local mode = u_mode.get_mode() or u_mode.modes["n"]
  local cmd = vim.api.nvim_eval_statusline("%S", {}).str
  -- if new_cmd == cmd then
  -- 	return padding
  -- end
  if cmd == "" or cmd == nil then
    if sl_options.showcmd_cmd_autoclear == true then
      cmd = ""
    else
      cmd = cmd_bak
    end
  end
  -- reset on 'escape'
  if cmd:sub(-2, -1) == "^[" then
    if sl_options.showcmd_cmd_clear_escape then
      cmd = ""
    else
      cmd = cmd_bak
    end
  end
  -- skip which-key
  if cmd:match("~@.*") then cmd = cmd_bak end
  -- skip ignore commands
  if vim.tbl_contains(sl_options.showcmd_ignore, cmd) then
    if sl_options.showcmd_cmd_clear_ignore and mode.name == "N" then
      cmd = ""
    else
      cmd = cmd_bak
    end
  end
  -- skip ignore leader commands
  for _, v in ipairs(sl_options.showcmd_ignore_leader) do
    -- escape special characters
    v = v:gsub("[%(%)%.%%%+%-%*%?%[%]%^%$]", "%%%1")
    if cmd:match(v) then cmd = "" end
  end
  -- replace <80>ü^D<20> (c-space) with '^␣'
  cmd = cmd:gsub("<80>ü^D<20>", "^␣")
  -- replace <20> (space) with '₋'
  cmd = cmd:gsub("<20>", "␣")
  -- escape '%' character
  if cmd == "%" then cmd = "%%" end
  cmd_bak = cmd
  local icon = sl_options.showcmd_icon
  if mode.name == "V" then icon = sl_options.showcmd_icon_visual end
  local s = icon .. cmd
  local padding = get_showcmd_padding()
  return utils.get_statuslineitem("BeSLineShowCmd", s .. padding .. " ", false)
end

local function collapse() return utils.get_statuslineitem("BeSLine", "%<", false) end

local function mid_sep() return utils.get_statuslineitem("BeSLine", "%=", false) end

function M.statusline()
  return ""
    .. statusline_mode()
    .. qf_infos()
    .. qf_index()
    .. git_infos()
    .. parent_dir()
    .. collapse()
    .. path()
    .. showcmd()
    .. mid_sep()
    .. macro_recording()
    .. search_count()
    .. lsp_diagnostics_item()
    .. filetype()
    .. lsp_status()
    .. progress()
end

return M
