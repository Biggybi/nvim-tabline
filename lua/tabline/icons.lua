local M = {}
local hl = require("tabline.highlights")
local options = require("tabline.config").options
local utils = require("tabline.utils")

function M.get_devicon(index, bufname, extension)
  local _, web_devicons = pcall(require, "nvim-web-devicons")
  local filename = vim.fn.fnamemodify(bufname, ":t")

  if not utils.has_icon() then return "" end
  local icon_image, icon_hl_group =
    web_devicons.get_icon(filename, extension, { default = true })

  local fg_color = hl.extract_color(icon_hl_group, "fg")
  icon_hl_group = "BeTLine" .. icon_hl_group

  local active_fg_color = hl.extract_color("BeTLineIconSel", "fg") or fg_color
  vim.api.nvim_set_hl(
    0,
    icon_hl_group .. "Sel",
    { fg = active_fg_color, bg = hl.extract_color("BeTLineSel", "bg") }
  )

  local inactive_color = (
    not options.color_all_icons and hl.get_tabline_colors().inactive_modified_bg
  )
    or hl.extract_color("BeTLineIcon", "fg")
    or fg_color

  vim.api.nvim_set_hl(
    0,
    icon_hl_group,
    { fg = inactive_color, bg = hl.extract_color("BeTLine", "bg") }
  )
  return utils.get_tabitem(icon_hl_group, icon_image .. " ", index)
end

return M
