local M = {}

function M.extract_color(group, attr)
  local color = vim.api.nvim_get_hl(0, { name = group })[attr]
  return color ~= "" and color or nil
end

function M.get_tabline_colors()
  return {
    bg = M.extract_color("Normal", "bg"),
    -- modified_fg = M.get_color("ErrorMsg", "fg"),
    modified_fg = M.extract_color("String", "fg"),
    modified_fg_nc = M.extract_color("BeSLineFileModNC", "fg"),
    gitdir_fg = M.extract_color("Normal", "bg"),
    gitdir_bg = M.extract_color("CursorLineNr", "fg"),
    dir_fg = M.extract_color("CustomTabLineSel", "fg"),
    dir_bg = M.extract_color("CustomTabLineSel", "bg"),
    tabactive_bg = M.extract_color("CustomTabLineSel", "bg"),
    tabinactive_bg = M.extract_color("CustomTabLine", "bg"),
  }
end

function M.get_fg(active)
  local group = active and "CustomTabLineSel" or "CustomTabLine"
  return M.extract_color(group, "fg")
end

function M.get_bg(active)
  local group = active and "CustomTabLineSel" or "CustomTabLine"
  return M.extract_color(group, "bg")
end

return M
