local M = {}
local fn = vim.fn
local utils = require("tabline.utils")
local icons = require("tabline.icons")
local options = require("tabline.config").options.tabline
local u_mode = require("tabline.utils.mode")

local function tabline_tab(index)
  local left_sep = options.left_separator
  local right_sep = options.right_separator
  local bufnr = fn.tabpagebuflist(index)[fn.tabpagewinnr(index)]
  local tabname = utils.get_tabname(bufnr)
  local tablabel = utils.get_label(tabname, index, utils.get_labelsize(), true)
  local file_extension = fn.fnamemodify(tabname, ":e")
  local active = index == fn.tabpagenr()
  local bufmodified = fn.getbufvar(bufnr, "&mod") == 1
  local margin = string.rep(" ", options.tab_margin)

  local tab_items = {
    utils.clickableTab(index),
    utils.get_tabitem("BeTLineSeparator", left_sep, active),
    icons.get_devicon(active, tabname, file_extension),
    utils.get_tabitem("BeTLine", tablabel, active, bufmodified),
    utils.get_tabitem("BeTLineSeparator", right_sep, active),
    utils.get_tabitem("BeTLineMargin", margin),
  }
  return table.concat(tab_items)
end

local function tabline_tabs()
  local s = ""
  local tabs_number = fn.tabpagenr("$")
  for index = 1, tabs_number do
    s = s .. tabline_tab(index)
  end
  return s
end

local function tabline_git_dir()
  local left_sep = options.left_separator
  local right_sep = options.right_separator
  local dirname =
    utils.get_label(fn.fnamemodify(fn.getcwd(), ":t"), 0, options.dir_section_size, false)
  local section_separation = "%="
  local is_git = utils.get_gitdir() ~= ""
  local gitdirhl = is_git and "BeTLineGitDir" or "BeTLineDir"
  local gitdirsephl = is_git and "BeTLineGitDirSep" or "BeTLineDirSep"
  local dir_icon = is_git and options.directory_icon.git or options.directory_icon.default
  local git_dir_items = {
    utils.get_tabitem("BeTLineSeparator", section_separation, false),
    utils.get_tabitem(gitdirsephl, left_sep, false),
    utils.get_tabitem(gitdirhl, dir_icon, false),
    utils.get_tabitem(gitdirhl, dirname, false),
    utils.get_tabitem(gitdirsephl, right_sep, false),
  }
  return table.concat(git_dir_items)
end

local function tabline_session()
  if not utils.loaded_obsession() then return "" end
  local obsession_status = utils.get_obsession()
  if not obsession_status then return "" end

  local left_sep = options.left_separator
  local right_sep = options.right_separator
  local mode = u_mode.get_mode() or u_mode.modes["n"]
  local color =
    vim.api.nvim_get_hl(0, { id = vim.api.nvim_get_hl_id_by_name(mode.color) })
  vim.api.nvim_set_hl(0, "BeTLineSessionSep", { fg = color.bg })
  -- vim.cmd("highlight BeTLineSessionSep guifg=" .. color.bg .. " guibg=NONE")
  local session_items = {
    utils.get_statuslineitem("TabLineFill", " "),
    utils.get_statuslineitem("BeTLineSession", left_sep, true),
    utils.get_statuslineitem(mode.color, obsession_status .. " ", false),
    utils.get_statuslineitem("BeTLineSession", right_sep, true),
  }
  return table.concat(session_items)
end

function M.tabline()
  local items = {
    tabline_tabs(),
    tabline_git_dir(),
    tabline_session(),
  }
  return table.concat(items)
end

return M
