local M = {}

---@param opts BeLineOptions
function M.setup(opts)
  require("tabline.config").setup(opts)
  require("tabline.besline").besline()
  require("tabline.commands").create_commands()
end

return M
