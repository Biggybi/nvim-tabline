local M = {}
local u_mode = require("tabline.utils.mode")

local function set_tabline()
  local options = require("tabline.config").options
  if options.tabline.reload_on_focus == true then
    vim.api.nvim_create_autocmd({
      "FocusGained",
    }, {
      group = vim.api.nvim_create_augroup("BeTlineActive", {}),
      pattern = "*",
      callback = function()
        vim.o.tabline = "%!v:lua.require('tabline.tabline').tabline()"
      end,
    })
  end
  vim.o.tabline = "%!v:lua.require('tabline.tabline').tabline()"
end

M.statusline_draw = {
  active = function()
    vim.b.besl_active = true
    -- dynamic statusline
    vim.wo.statusline = "%!v:lua.require('tabline.statusline').statusline()"
  end,
  inactive = function()
    vim.b.besl_active = false
    -- static statusline
    vim.wo.statusline = require("tabline.statusline").statusline()
  end,
}

local function set_statusline()
  local options = require("tabline.config").options
  vim.api.nvim_create_autocmd("User", {
    group = vim.api.nvim_create_augroup("BeSlineActiveGitSigns", {}),
    pattern = "GitSignsUpdate",
    callback = M.statusline_draw.active,
  })

  vim.api.nvim_create_autocmd(options.statusline.reload_events_active, {
    group = vim.api.nvim_create_augroup("BeSlineActive", {}),
    pattern = "*",
    callback = M.statusline_draw.active,
  })

  vim.api.nvim_create_autocmd(options.statusline.reload_events_inactive, {
    group = vim.api.nvim_create_augroup("BeSlineInactive", {}),
    pattern = "*",
    callback = M.statusline_draw.inactive,
  })
end

local function set_extras()
  local options = require("tabline.config").options
  local highlights = require("tabline.highlights")

  if options.extras.mode_sync_hl == nil then return end

  ---@param mode Mode
  local function mode_hl_sync(mode)
    local fg = highlights.extract_color(mode.color, "bg")
    for _, hl in ipairs(options.extras.mode_sync_hl) do
      local bg = highlights.extract_color(hl, "bg")
      vim.api.nvim_set_hl(0, hl, { fg = fg, bg = bg })
    end
  end

  vim.api.nvim_create_autocmd({ "ModeChanged", "InsertLeave" }, {
    group = vim.api.nvim_create_augroup("BesExtraModeSync", {}),
    pattern = "*",
    callback = function() mode_hl_sync(u_mode.get_mode() or u_mode.modes["n"]) end,
  })
  vim.api.nvim_create_autocmd({ "ModeChanged" }, {
    group = vim.api.nvim_create_augroup("BesExtraNormalModeSync", {}),
    pattern = "*:n",
    callback = function() mode_hl_sync(u_mode.modes["n"]) end,
  })
end

local function statuscolumn()
  local options = require("tabline.config").options
  if vim.tbl_contains(options.statuscolmn.ft_blacklist, vim.bo.filetype) then return end
  vim.wo.statuscolumn = "%{%v:lua.require('tabline.statuscolumn').statuscolumn()%}"
end

local function set_statuscolumn()
  vim.api.nvim_create_autocmd("FileType", {
    group = vim.api.nvim_create_augroup("BesStatusColumn", {}),
    pattern = "*",
    callback = statuscolumn,
  })

  vim.api.nvim_create_autocmd("OptionSet", {
    group = vim.api.nvim_create_augroup("BesStatusColumnScreenLine", {}),
    pattern = "cursorlineopt",
    callback = statuscolumn,
  })
end

function M.besline()
  set_tabline()
  set_statusline()
  set_extras()
  set_statuscolumn()
end

return M
