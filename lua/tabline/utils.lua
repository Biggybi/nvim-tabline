local M = {}
local options = require("tabline.config").options.tabline
local fn = vim.fn

function M.clickableTab(index) return "%" .. index .. "T" end

function M.get_gitdir()
  local ok, dot_git_path = pcall(fn.finddir, ".git", ".;")
  if not ok then return "" end
  return dot_git_path
end

function M.is_special_buffer()
  if
    vim.bo.buftype == nil
    or vim.bo.buftype ~= ""
    or vim.bo.buftype == "nofile"
    or vim.bo.filetype == "gitcommit"
  then
    return true
  end
  return false
end

function M.loaded_obsession() return vim.g.loaded_obsession == 1 end

function M.get_obsession()
  if not M.loaded_obsession() then return nil end
  local obsession_status =
    vim.fn.ObsessionStatus(options.session_icon.active, options.session_icon.inactive)
  if obsession_status == "" then return nil end
  return obsession_status
end

function M.get_iconsize()
  local left_sep = options.left_separator
  local right_sep = options.right_separator
  return #left_sep + #right_sep
end

function M.session_section_size()
  local indicator_size = 2
  local separator_size = 2
  local margin = options.tab_margin
  return indicator_size + separator_size * 2 + margin
end

function M.tab_overflow()
  local max_tab_size = options.tab_size
  local tab_margin = options.tab_margin
  local tab_count = fn.tabpagenr("$")
  local rhs_size = M.rhs_size()
  local total_size = (tab_count * (max_tab_size + tab_margin))
  total_size = total_size + rhs_size
  return total_size > vim.o.columns
end

function M.get_tabname(bufnr)
  if fn.bufname(bufnr) ~= "" then return fn.bufname(bufnr) end
  if vim.o.filetype ~= "" then return fn.getbufvar(bufnr, "&filetype") end
  if vim.o.buftype ~= "" then return vim.o.buftype end
  return options.no_name
end

function M.rhs_size()
  local session_size = M.session_section_size()
  local dir_size = options.dir_section_size + 1 -- +1 for the icon
  return session_size and session_size + dir_size or dir_size
end

function M.get_tabsize()
  local max_tab_size = options.tab_size
  if not M.tab_overflow() then return max_tab_size end

  local rhs_size = M.rhs_size()
  local tab_count = fn.tabpagenr("$")
  return math.floor((vim.o.columns - rhs_size) / tab_count)
end

function M.get_labelsize()
  local size = M.get_tabsize()
  local icon_size = 4
  local icon_margin = 1
  if M.has_icon() then size = size - icon_size - icon_margin end
  return size
end

function M.get_tabitem(group, item, active, modified)
  if modified then group = group .. "Modified" end
  if active then group = group .. "Sel" end
  return "%#" .. group .. "#" .. item .. "%*"
end

function M.get_statuslineitem(group, item, sep, mod)
  if sep then group = group .. "Sep" end
  if mod then group = group .. "Mod" end
  if vim.b.besl_active == false then group = group .. "NC" end
  return "%#" .. group .. "#" .. item .. "%*"
end

function M.label_truncate(label, label_size)
  local filename = fn.fnamemodify(label, ":t")
  if filename and filename ~= "" then label = filename end
  if string.match(label, "^fugitive:///") then label = "fugitive" end
  if label and #label > label_size then label = label:sub(1, label_size - 1) .. "`" end
  return label
end

function M.has_icon()
  local enabled = options.show_icon
  local ok = pcall(require, "nvim-web-devicons")
  return enabled and ok
end

function M.label_padd(label, label_size, use_icon)
  local has_icon = M.has_icon()
  local left_padding = math.floor((label_size - #label) / 2)
  local right_padding = label_size - #label - left_padding

  if use_icon and has_icon and left_padding > 0 then
    right_padding = right_padding + 1
    left_padding = left_padding - 1
  end

  local padded_name = string.rep(" ", left_padding)
    .. label
    .. string.rep(" ", right_padding)
  return padded_name
end

function M.get_label(label, index, label_size, use_icon)
  local title = fn.gettabvar(index, "TablineTitle")
  if title ~= vim.NIL and title ~= "" then label = title end
  if label == "" then label = options.no_name end

  label = M.label_truncate(label, label_size)
  label = M.label_padd(label, label_size, use_icon)

  return label
end

return M
