local M = {}

local options = require("tabline.config").options.statuscolmn
local utils = require("tabline.utils")

function M.right_align(s)
  local pad = math.max(#tostring(vim.fn.line("$")), options.min_number_width)
    - vim.fn.strchars(s)
  return string.rep(" ", pad) .. s
end

function M.get_line_number()
  if vim.wo.number and vim.wo.relativenumber then
    if vim.v.lnum == vim.fn.line(".") then
      return vim.v.lnum
    else
      return vim.fn.abs(vim.fn.line(".") - vim.v.lnum)
    end
  end

  if vim.wo.relativenumber then return vim.fn.abs(vim.fn.line(".") - vim.v.lnum) end

  return vim.v.lnum
end

function M.is_last_of_group()
  local line_length = vim.fn.virtcol({ vim.v.lnum, "$" })
  local text_line_width = vim.fn.winwidth(0)
    - vim.fn.getwininfo(vim.api.nvim_get_current_win())[1].textoff
  return text_line_width * (vim.v.virtnum + 1) >= line_length - 1
end

function M.is_first_of_group() return vim.v.virtnum == 0 end

function M.wrap_symbol()
  return M.is_last_of_group() and options.wrap_symbol_last or options.wrap_symbol
end

function M.symbol(symbol)
  return string.format("%" .. options.min_number_width .. "s", M.right_align(symbol))
end

function M.statuscolumn()
  local cursorlineopt = vim.opt.cursorlineopt:get()

  local cursorline_number = vim.tbl_contains(cursorlineopt, "number")
    or vim.tbl_contains(cursorlineopt, "both")
  local cursorline_screenline = vim.tbl_contains(cursorlineopt, "screenline")
    or vim.tbl_contains(cursorlineopt, "both")
  local cursorline_line = vim.tbl_contains(cursorlineopt, "line")

  local sign = "%s"
  if utils.is_special_buffer() then sign = "" end

  if not vim.wo.number and not vim.wo.relativenumber then return sign end

  local is_first_of_group = M.is_first_of_group()
  local is_current_line = vim.v.lnum == vim.fn.line(".")

  local left_sep = " "
  local right_sep = " "
  if is_first_of_group and is_current_line then
    left_sep = utils.get_tabitem("Normal", " ", false)
    right_sep = utils.get_tabitem("Normal", " ", false)
    if cursorline_number then
      left_sep = utils.get_tabitem("CursorLineSep", options.left_separator, false)
      right_sep = utils.get_tabitem("CursorLineSep", options.right_separator, false)
    end
  end

  local symbol =
    M.symbol(is_first_of_group and tostring(M.get_line_number()) or M.wrap_symbol())

  symbol = utils.get_tabitem(
    cursorline_number and "BeNLineNr" or "Normal",
    symbol,
    not (is_current_line and is_first_of_group)
  )

  local right_padd = utils.get_tabitem("Normal", " ", false)

  local cursorline_init_sep = (
    is_first_of_group
    and is_current_line
    and (cursorline_line or cursorline_screenline)
  )
      and utils.get_tabitem("CursorLineSep", options.left_separator, false)
    or ""

  return sign .. left_sep .. symbol .. right_sep .. right_padd .. cursorline_init_sep
end

return M
