local M = {}

function M.create_commands()
  vim.api.nvim_create_user_command(
    "Tabname",
    function(opts) require("tabline.actions").set_tabname(opts.fargs[1]) end,
    {
      nargs = "?", -- 0 or 1
      desc = "Rename Tab",
    }
  )

  vim.api.nvim_create_user_command(
    "TabnameReset",
    require("tabline.actions").reset_tabname,
    {
      desc = "Reset Tabname",
    }
  )
end

local function clear_autocmds()
  for _, group in pairs({
    "BeSlineActiveGitSigns",
    "BeSlineActive",
    "BeSlineInactive",
    "BesExtraModeSync",
  }) do
    pcall(function() vim.api.nvim_del_augroup_by_name(group) end)
  end
end

vim.api.nvim_create_user_command(
  "BeSlineActive",
  function() require("tabline").statusline_draw.active() end,
  {
    desc = "Enable active statusline",
  }
)

vim.api.nvim_create_user_command(
  "BeSlineInactive",
  function() require("tabline").statusline_draw.inactive() end,
  {
    desc = "Enable inactive statusline",
  }
)

vim.api.nvim_create_user_command("BeSlineEnable", function()
  require("tabline").statusline_draw.inactive()
  require("tabline").statusline_draw.active()
  require("tabline").setup()
end, {
  desc = "Enable BeSline",
})

vim.api.nvim_create_user_command("BeSlineDisable", function()
  clear_autocmds()
  -- vim.o.statusline = ""
  vim.cmd("windo set statusline=")
end, {
  desc = "Disable BeSline",
})

vim.api.nvim_create_user_command("BeSlineClear", function()
  clear_autocmds()
  vim.o.tabline = ""
  vim.cmd("windo set statusline=")
end, {
  desc = "Clear BeSline",
})

vim.api.nvim_create_user_command("BesLinePause", function() clear_autocmds() end, {
  desc = "Clear BeSline autocmds",
})

vim.api.nvim_create_user_command("BesLine", function() require("tabline").setup() end, {
  desc = "Setup BeSline",
  complete = function(_, cmd_line, _)
    local commands = {
      enable = { "tabline", "statusline" },
      disable = { "tabline", "statusline" },
      toggle = { "tabline", "statusline" },
      clear = { "tabline", "statusline" },
      pause = { "tabline", "statusline" },
      resume = { "tabline", "statusline" },
    }
    local command_names = vim.tbl_keys(commands)
    if cmd_line == "BesLine " then return command_names end
    -- if command-line is "BesLine <command> ""
    if cmd_line:match("BesLine %w+ +") then
      -- check if last word is a command
      local last_word = cmd_line:match("(%w+)%s+$")
      if vim.tbl_contains(command_names, last_word) then return commands[last_word] end
    end
  end,
  nargs = "*",
})
return M
