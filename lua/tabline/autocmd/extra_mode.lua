local M = {}

M.mode = ""

-- TODO: implement user autocmd
function M.get_mode_extra()
  local mode = vim.api.nvim_get_mode().mode
  if mode == M.mode then return end
  if mode == nil then return mode_code end
  local statusline = vim.o.statusline
  local statusline_mode = statusline:match("%%S(.-)%%")
  if statusline_mode == nil then return mode end
  M.mode = mode
  return statusline_mode
end

return M
