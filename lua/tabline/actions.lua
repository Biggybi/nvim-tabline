local M = {}

function M.set_tabname(name)
  if not name then
    vim.ui.input({ prompt = "Tabname: " }, function(input)
      if not input then return end
      name = input
    end)
  end
  local tabnr = vim.fn.tabpagenr()
  vim.fn.settabvar(tabnr, "TablineTitle", name)
  vim.cmd("redrawtabline")
end

function M.reset_tabname()
  local tabnr = vim.fn.tabpagenr()
  vim.fn.settabvar(tabnr, "TablineTitle", "")
  vim.cmd("redrawtabline")
end

return M
